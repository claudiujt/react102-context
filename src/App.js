import logo from './logo.svg';
import ThemeContext from "./ThemeContext";
import { useContext, useEffect, useState } from "react";
import ThemeSelect from "./components/ThemeSelect";
import ElementsColorSelect from "./components/ElementsColorSelect";
import Card from "./components/Card";
import Input from "./components/Input";
import Table from "./components/Table";
import Select from "./components/Select";
import Button from "./components/Button";
import "./App.css";
import "./elements.css";

function App() {
    const [themes, setThemes] = useState({
        appTheme: "dark",
        elementsColor: "blue",
    });

    const changeAppTheme = (e) =>
        setThemes({ ...themes, appTheme: e.target.value });
    const changeElementsColor = (e) =>
        setThemes({ ...themes, elementsColor: e.target.value });

    return (
        <ThemeContext.Provider
            value={{
                themes,
                changeAppTheme,
                changeElementsColor,
            }}
        >
            <div
                className={`App ${
                    themes.appTheme == "dark" ? "bg-dark" : "bg-light"
                }`}
            >
                <header
                    className={`App-header ${
                        themes.appTheme == "dark" ? "bg-dark" : "bg-light"
                    }`}
                >
                    <h1>Assignment #1</h1>
                    <h3>Claudiu J</h3>
                </header>
                <main>
                    <div className="container">
                        <ThemeSelect />
                        <ElementsColorSelect />
                        <h3>Components</h3>
                        <div className="components">
                            <Card />
                            <Input />
                            <Table />
                            <Button />
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="48"
                                height="48"
                                fill={
                                    themes.elementsColor == "blue"
                                        ? "#0F044C"
                                        : "#FFD700"
                                }
                                className="bi bi-brightness-high-fill"
                                viewBox="0 0 16 16"
                            >
                                <path d="M12 8a4 4 0 1 1-8 0 4 4 0 0 1 8 0zM8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0zm0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13zm8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0zm-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zm9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707zM4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708z" />
                            </svg>
                        </div>
                    </div>
                </main>
            </div>
        </ThemeContext.Provider>
    );
}

export default App;
