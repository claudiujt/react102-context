import { useContext } from "react";
import ThemeContext from "../ThemeContext";

function ElementsColorSelect() {
    const { themes, changeElementsColor } = useContext(ThemeContext);

    return (
        <div className="custom-select">
            <label htmlFor="colorSelect">Toggle Blue/Yellow Colors</label>
            <select
                id="colorSelect"
                onChange={changeElementsColor}
                value={themes.elementsColor}
                className={
                    themes.elementsColor == "blue" ? "bg-blue" : "bg-yellow"
                }
            >
                <option value="blue">Blue</option>
                <option value="yellow">Yellow</option>
            </select>
        </div>
    );
}

export default ElementsColorSelect;
