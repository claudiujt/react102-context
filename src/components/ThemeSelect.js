import { useContext } from "react";
import ThemeContext from "../ThemeContext";

function ThemeSelect() {
    const { themes, changeAppTheme } = useContext(ThemeContext);

    return (
        <div className="custom-select">
            <label htmlFor="themeSelect">Toggle Dark/Light Theme</label>
            <select
                id="themeSelect"
                onChange={changeAppTheme}
                value={themes.appTheme}
                className={themes.appTheme == "dark" ? "bg-dark" : "bg-light"}
            >
                <option value="dark">Dark</option>
                <option value="light">Light</option>
            </select>
        </div>
    );
}

export default ThemeSelect;
