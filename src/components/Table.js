import { useContext } from "react";
import ThemeContext from "../ThemeContext";

function Table() {
    const { themes } = useContext(ThemeContext);

    return (
        <table className="table">
            <thead
                className={themes.appTheme == "dark" ? "bg-dark" : "bg-light"}
            >
                <tr>
                    <th>First Column</th>
                    <th>Second Column</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Data</td>
                    <td>Data</td>
                </tr>
                <tr>
                    <td>Data</td>
                    <td>Data</td>
                </tr>
                <tr>
                    <td>Data</td>
                    <td>Data</td>
                </tr>
            </tbody>
        </table>
    );
}

export default Table;
