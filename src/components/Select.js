import { useContext } from "react";
import ThemeContext from "../ThemeContext";

function Select() {
    const { themes } = useContext(ThemeContext);

    return (
        <select
            className={`select ${
                themes.appTheme == "dark" ? "bg-dark" : "bg-light"
            }`}
        >
            <option>Select Input</option>
            <option value="a">A</option>
            <option value="b">B</option>
            <option value="c">C</option>
        </select>
    );
}

export default Select;
