import { useContext } from "react";
import ThemeContext from "../ThemeContext";

function Button() {
    const { themes } = useContext(ThemeContext);

    return (
        <button
            className={`btn ${
                themes.elementsColor == "blue" ? "bg-blue" : "bg-yellow"
            }`}
        >
            Button
        </button>
    );
}

export default Button;
