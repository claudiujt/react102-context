import { useContext } from "react";
import ThemeContext from "../ThemeContext";

function Card() {
    const { themes } = useContext(ThemeContext);

    return (
        <div
            className={`card ${
                themes.appTheme == "dark" ? "bg-dark" : "bg-light"
            }`}
        ></div>
    );
}

export default Card;
