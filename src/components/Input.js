import { useContext } from "react";
import ThemeContext from "../ThemeContext";

function Input() {
    const { themes } = useContext(ThemeContext);

    return (
        <>
            <input
                type="text"
                className={`form-control ${
                    themes.appTheme == "dark" ? "bg-dark" : "bg-light"
                }`}
            />
        </>
    );
}

export default Input;
